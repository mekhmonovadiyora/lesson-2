
// Factory Function

function productFactory(name, info, price) {
    product = {
        name,
        info,
        price,
    }
    return Object.values(product)
}

const product1 = productFactory('Макси Бокс "Популярный"', 'лаваш мясной classic, картофель-фри, пепси 0,25 л, *соус на выбор, вид соуса просим указать в комментарии', 35000)

// Function Construction

function Product(name, info, price) {
    this.name = name
    this.info = info
    this.price = price
}

const product2 = new Product('Макси Бокс "Популярный"', 'лаваш мясной classic, картофель-фри, пепси 0,25 л, *соус на выбор, вид соуса просим указать в комментарии', 30000)

console.log(product1)
console.log(product2)


